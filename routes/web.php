<?php

use Illuminate\Support\Facades\Route;
use app\Http\Controllers\UploadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/upload',[App\Http\Controllers\FileController::class, 'Create'])->name('upload')->middleware('auth');
Route::get('/showfiles',[App\Http\Controllers\FileController::class, 'index'])->name('showfiles')->middleware('auth');
Route::post('/upload',[App\Http\Controllers\FileController::class, 'Store'])->name('upload')->middleware('auth');
