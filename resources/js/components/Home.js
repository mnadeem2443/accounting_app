import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

function Home(){
    return (
        <Router>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Dashboard </div>
                            <div class="card-body">
                                <h5>Files</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="/upload">
                                            <button class="btn btn-primary form-control">Upload a File</button>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="/showfiles">
                                            <button class="btn btn-primary form-control">Show all Files</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Router>
    );
}

export default Home;

if (document.getElementById('Home')) {
    ReactDOM.render(<Home />, document.getElementById('Home'));
}


// <Link to='/home'>Home</Link>
// <Link to='/users'>Users</Link>
//     <Switch>
//         <Route path='/home'>
//             <Home/>
//         </Route>

//         <Route path='/users'>
//             <Users/>
//         </Route>
//     </Switch>   