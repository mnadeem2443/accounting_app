import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

function Welcome(){
    return (
        <Router>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <h1>Welcome to the Accouting App</h1>
                        <p>Please <Link to='/login'>Login</Link> or <Link to='/register'>Register</Link> to get started</p>
                        <p>If you are already logged in goto <Link to='/home'>Dashboard</Link></p>
                    </div>
                </div>
            </div>
        </Router>
    );
}

export default Welcome;

if (document.getElementById('welcome')) {
    ReactDOM.render(<Welcome />, document.getElementById('welcome'));
}


// <Link to='/home'>Home</Link>
// <Link to='/users'>Users</Link>
//     <Switch>
//         <Route path='/home'>
//             <Home/>
//         </Route>

//         <Route path='/users'>
//             <Users/>
//         </Route>
//     </Switch>   