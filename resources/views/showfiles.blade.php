@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Uploaded Files') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($files->count() > 0)
                    <table class='table'>
                        <th>ID</th>
                        <th>File Name</th>
                        <th>Uploaded by</th>

                        @foreach ($files as $file)
                        <tr>
                            <td> {{ $file->id }} </td>
                            <td> {{ $file->filename }} </td>
                            <td> {{ $users->find( $file->userid )->name }} </td>
                        </tr>
                        @endforeach
                    </table>

                    @else

                    {{ __('No file in your record') }}

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
